# Awesome authentication project

The project has some setup but still requires some efforts to make it awesome. Therefore, we need someone to help us to make it production ready.

Note: This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Site components

Basic flow of the site is as follows:

- Login page: Enter credentials to login. Credentials checked with local storage.
- Register page: Form validation is present and the user info is stored in local storage.
- Home page: Main page. Redirect to login page if not logged in.

## Deployment

Deployed using netlify: [link](https://okhati-exercise-sk462.netlify.app/)

## Steps to run in local machine

- Run app locally in development mode using `yarn && yarn start`
- Build the app for production to the build folder using `yarn build`.
