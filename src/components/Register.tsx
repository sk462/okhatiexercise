import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";

import { useStyles } from "../styles/Styles";
import { Link, useHistory } from "react-router-dom";

const validateEmail = (email: String) => {
  //Use regex to validate email
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
};

const validatePassword = (text: string) => {
  if (text.length < 8) return false;
  //Check if it has at least a digit and a character
  if (!/\d/.test(text) && /[a-zA-Z]/.test(text)) return false;

  return true;
};

export const Register: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  //To not display error messages when user is entering the fields at first
  const [isFirstPass, setIsFirstPass] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  //Handle submission of form
  const onFormSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    setIsFirstPass(false);
    if (
      validateEmail(email) &&
      validatePassword(password) &&
      password === confirmPassword
    ) {
      //Submit the form
      console.log("Valid form");

      //Practically, the data is sent to the backend and user is redirected to login page on success
      //For the exercise, a user is created in localstorage
      //Added delay for real effect
      const user = {
        email,
        password,
      };
      setIsLoading(true);
      setTimeout(() => {
        //Slow operation
        localStorage.setItem("user", JSON.stringify(user));
        history.push("login", { userCreatedMsg: "User created successfully" });
        setIsLoading(false);
      }, 500);
    }
  };

  //Main component
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}></Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>

        <form className={classes.form}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={(e) => setEmail(e.target.value)}
                error={!isFirstPass && !validateEmail(email)}
                helperText={
                  !isFirstPass && !validateEmail(email)
                    ? "Enter valid email address"
                    : ""
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
                error={!isFirstPass && !validatePassword(password)}
                helperText={
                  !isFirstPass && !validatePassword(password)
                    ? "Password must be 8 characters with at least one letter and number"
                    : ""
                }
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirm-password"
                label="Confirm Password"
                type="password"
                id="confirm-password"
                onChange={(e) => setConfirmPassword(e.target.value)}
                error={!isFirstPass && password !== confirmPassword}
                helperText={
                  !isFirstPass && password !== confirmPassword
                    ? "Passwords do not match"
                    : ""
                }
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onFormSubmit}
            disabled={isLoading}
          >
            {isLoading ? <CircularProgress /> : "Register"}
          </Button>

          <Grid item>
            <Link to="/login">Already have an account? Log in</Link>
          </Grid>
        </form>
      </div>
    </Container>
  );
};
