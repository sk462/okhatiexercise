import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import CircularProgress from "@material-ui/core/CircularProgress";

import { useStyles } from "../styles/Styles";
import { AlertMessage } from "./AlertMessage";
import { Link } from "react-router-dom";
import { History } from "history";
import Auth from "../auth/Auth";

interface LoginProps {
  history: History;
  /* other props for ChildComponent */
}

export const Login: React.FC<LoginProps> = (props) => {
  const classes = useStyles();
  const [status, setStatus] = useState<{ key: string; msg: string }>();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [refresh, setRefresh] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  //If redirected from the register page, show the notification message
  useEffect(() => {
    console.log(props.history.location);
    const userCreated = props.history.location.state as any;
    if (userCreated !== undefined && userCreated.userCreatedMsg) {
      setStatus({
        key: Math.random().toString(),
        msg: userCreated.userCreatedMsg,
      });
      setRefresh(!refresh);
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  //Handle login submission
  const onFormSubmit = (e: React.MouseEvent) => {
    e.preventDefault();

    //Added delay for real effect
    setIsLoading(true);
    setTimeout(() => {
      //Verify the credentials
      const userJson = localStorage.getItem("user");
      if (userJson) {
        const user = JSON.parse(userJson);
        if (email === user.email && password === user.password) {
          console.log("valid");
          Auth.login(() => {
            props.history.push("/");
            console.log("Logged in successfully");
          });
          return;
        }
      }

      setIsLoading(false);
      setStatus({
        key: Math.random().toString(),
        msg: "Invalid credentials",
      });
      setRefresh(!refresh);
    }, 500);
  };

  //Main component
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      {/* <AlertMessage message={"Testing   "} /> */}
      {status ? <AlertMessage message={status.msg} refresh={refresh} /> : null}

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>{/* <LockOutlinedIcon /> */}</Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form className={classes.form}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onFormSubmit}
            disabled={isLoading}
          >
            {isLoading ? <CircularProgress /> : "Login"}
          </Button>

          <Grid item xs>
            <Link to="/register">{"No account? Register here"}</Link>
          </Grid>
        </form>
      </div>
    </Container>
  );
};
