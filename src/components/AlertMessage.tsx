import React, { useState, useEffect } from "react";

import Snackbar from "@material-ui/core/Snackbar";
// import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";

interface AlertMessageProps {
  message: string;
  refresh: boolean;
}

export const AlertMessage: React.FC<AlertMessageProps> = ({
  message,
  refresh,
}) => {
  const [open, setOpen] = useState(true);

  const handleClose = (event: React.SyntheticEvent<any, Event>) => {
    setOpen(false);
  };

  useEffect(() => {
    setOpen(true);
  }, [refresh]);

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        open={open}
        autoHideDuration={2000}
        onClose={handleClose}
        message={message}
        action={[
          <IconButton color="secondary" key="close" onClick={handleClose}>
            x
          </IconButton>,
        ]}
      ></Snackbar>
    </div>
  );
};
