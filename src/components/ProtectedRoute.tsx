import React from "react";
import { Route, Redirect } from "react-router-dom";
import Auth from "../auth/Auth";

interface ProtectedRouteProps {
  component: any;
  [rest: string]: any;
}

export const ProtectedRoute: React.FC<ProtectedRouteProps> = ({
  component: Component,
  ...rest
}) => {
  //If authenticated, render the route else redirect to login page
  return (
    <Route
      {...rest}
      render={(props) => {
        if (Auth.isAuthenticated()) return <Component {...props} />;
        else
          return (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            ></Redirect>
          );
      }}
    />
  );
};
