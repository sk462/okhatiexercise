import { Button, Typography } from "@material-ui/core";
import React from "react";
import Auth from "../auth/Auth";
import { History } from "history";
import { useStyles } from "../styles/Styles";

interface HomeProps {
  history: History;
}

export const Home: React.FC<HomeProps> = (props) => {
  const classes = useStyles();
  return (
    <div>
      <Typography component="h1" variant="h5" className={classes.heading}>
        Welcome to the home page
      </Typography>
      <Button
        variant="contained"
        color="default"
        onClick={(e) => {
          e.preventDefault();
          Auth.logout(() => {
            props.history.push("/login");
            console.log("Logged out successfully");
          });
        }}
      >
        Logout
      </Button>
    </div>
  );
};
