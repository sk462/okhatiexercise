import React from "react";
import "./App.css";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { Login } from "./components/Login";
import { Register } from "./components/Register";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./components/Home";
import { ProtectedRoute } from "./components/ProtectedRoute";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#131b23",
    },
    secondary: {
      main: "#567a9f",
    },
  },
});

const App: React.FC = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Router>
        <div className="App">
          <Switch>
            <Route path="/login" exact component={Login}></Route>
            <Route path="/register" exact component={Register}></Route>
            <ProtectedRoute path="/" exact component={Home}></ProtectedRoute>
          </Switch>
        </div>
      </Router>
    </MuiThemeProvider>
  );
};

export default App;
